using System;
using PGSTestProject.Utils;
using UnityEngine;
using PGSTestProject.Model;

namespace PGSTestProject.Controllers
{
    public class GrenadeThrowController : MonoBehaviour
    {
        public event Action OnTrowGrenade;

        [SerializeField] private Transform pointPosition;
        [SerializeField] private LayerMask ignoreLayers;
        [SerializeField] private LineRenderer trajectoryLine;
        [SerializeField] private Transform grenadeSpawnPosition;
        [SerializeField] private Transform cameraTransform;
        [Space]
        [SerializeField] private GrenadeController grenadePrefab;

        private bool drawTrajectoryLine = false;
        private float throwAngle;
        private Vector3 throwDirection;
        private ViewSettings viewSettings;
        private Settings settings;
        private GameObjectsPoolController gameObjectsPoolController;
        private IGameState gameState;

        private void Start()
        {
            settings = SimpleInjector.Get<Settings>();
            gameState = SimpleInjector.Get<IGameState>();
            viewSettings = SimpleInjector.Get<ViewSettings>();
            gameObjectsPoolController = SimpleInjector.Get<GameObjectsPoolController>();
        }

        private void Update()
        {
            if (gameState.SelectedGrenade == GrenadeType.None) return;
            if (Input.GetMouseButtonDown(0)) drawTrajectoryLine = true;

            if (Input.GetMouseButtonUp(0))
            {
                if (!drawTrajectoryLine) return;
                drawTrajectoryLine = false;
                SpawnAndTrowGrenade();
            }
        }

        private void LateUpdate()
        {
            if (!drawTrajectoryLine)
            {
                trajectoryLine.gameObject.SetActive(false);
                return;
            }

            RaycastHit hit;
            Ray forwardRay = new Ray(cameraTransform.position, cameraTransform.forward);

            if (Physics.Raycast(forwardRay, out hit, viewSettings.TrajectoryRayDistance, ~ignoreLayers))
            {
                pointPosition.position = hit.point;
                trajectoryLine.gameObject.SetActive(true);
                RenderTrajectory(grenadeSpawnPosition.position, hit.point);
            }
            else
            {
                trajectoryLine.gameObject.SetActive(false);
            }
        }

        //Formulas https://en.wikipedia.org/wiki/Projectile_motion#Angle_.CE.B8_required_to_hit_coordinate_.28x.2Cy.29
        private void RenderTrajectory(Vector3 startPosition, Vector3 endPosition)
        {
            throwDirection = endPosition - startPosition;

            Vector3[] positions = new Vector3[viewSettings.TrajectoryNumberOfPoints + 1];

            Vector2 direction2d = new Vector2(Vector3.Distance(startPosition,
                new Vector3(endPosition.x, startPosition.y, endPosition.z)), endPosition.y - startPosition.y);

            var g = (-Physics.gravity.y);
            var v = settings.GrenadeThrowingForce;

            throwAngle = Mathf.Atan((v * v - Mathf.Sqrt(Mathf.Pow(v, 4) -
                g * (g * direction2d.x * direction2d.x + 2 * direction2d.y * v * v))) / (g * direction2d.x));

            if (float.IsInfinity(throwAngle) || float.IsNaN(throwAngle))
            {
                trajectoryLine.gameObject.SetActive(false);
                return;
            }

            float step = 1f / viewSettings.TrajectoryNumberOfPoints;
            float xStep = direction2d.x / viewSettings.TrajectoryNumberOfPoints;

            for (int i = 0; i <= viewSettings.TrajectoryNumberOfPoints; i++)
            {
                float x = xStep * i;
                float y = x * Mathf.Tan(throwAngle) - ((g * x * x) / (2 * v * v * Mathf.Cos(throwAngle) * Mathf.Cos(throwAngle)));
                Vector3 newPos = throwDirection * (step * i) + startPosition;
                newPos.y = startPosition.y + y;
                positions[i] = newPos;
            }
            this.trajectoryLine.positionCount = viewSettings.TrajectoryNumberOfPoints + 1;
            trajectoryLine.SetPositions(positions);
        }

        private void SpawnAndTrowGrenade()
        {
            if (gameState.SelectedGrenade == GrenadeType.None) return;

            var viewDataGrenade = viewSettings.GetGrenadeViewDataByType(gameState.SelectedGrenade);
            var grenade = gameObjectsPoolController.CreateGameObject(grenadePrefab);
            grenade.SetData(viewDataGrenade.Color, gameState.SelectedGrenade);
            grenade.OnBumped += GrenadeOnBumped;
            grenade.transform.position = grenadeSpawnPosition.position;

            float yDirection = Mathf.Tan(throwAngle);
            throwDirection.y = 0;
            throwDirection = throwDirection.normalized;
            Vector3 finalDirection = new Vector3(throwDirection.x, yDirection, throwDirection.z);
            grenade.Rigidbody.velocity = settings.GrenadeThrowingForce * finalDirection.normalized;

            OnTrowGrenade?.Invoke();
        }

        private void GrenadeOnBumped(GrenadeController grenadeController)
        {
            grenadeController.OnBumped -= GrenadeOnBumped;
            gameObjectsPoolController.RemoveGameObject(grenadeController);
        }
    }
}