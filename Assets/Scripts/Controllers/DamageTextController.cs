
using System;
using TMPro;
using UnityEngine;

namespace PGSTestProject.Controllers
{
    [RequireComponent(typeof(TextMeshPro))]
    public class DamageTextController : MonoBehaviour
    {
        public event Action<DamageTextController> OnRemove;

        [SerializeField] private float lifetime = 1f;

        private TextMeshPro text;
        private Transform cameraTransform;

        private void Awake()
        {
            text = GetComponent<TextMeshPro>();
            cameraTransform = Camera.main.transform;
        }

        private void Update()
        {
            transform.rotation = cameraTransform.rotation;;
            transform.position += Vector3.up * Time.deltaTime;
        }

        public void Show(int damage)
        {
            text.text = $"-{damage}";

            Invoke("Remove", lifetime);
        }

        private void Remove()
        {
            OnRemove?.Invoke(this);
        }
    }
}