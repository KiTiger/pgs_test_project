using System;
using UnityEngine;

namespace PGSTestProject.Controllers
{
    public class EnemyController : MonoBehaviour
    {
        public event Action<EnemyController> OnRemove;
        public event Action<EnemyController, int> OnDamage;

        private int currentHp;
        private int indexPosition;

        public int IndexPosition => indexPosition;

        public void SetData(int hp, int indexPosition)
        {
            currentHp = hp;
            this.indexPosition = indexPosition;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Explosion")
            {
                Damage(other.gameObject.GetComponent<ExplosionController>());
            }
        }

        private void Damage(ExplosionController explosionController)
        {
            currentHp -= explosionController.Damage;

            OnDamage.Invoke(this, explosionController.Damage);

            if (currentHp <= 0)
            {
                OnRemove?.Invoke(this);
            }
        }
    }
}