using System.Collections.Generic;
using PGSTestProject.Messages;
using PGSTestProject.Utils;
using UnityEngine;
using PGSTestProject.Model;

namespace PGSTestProject.Controllers
{
    public class EnemySpawnController : MonoBehaviour
    {
        [SerializeField] private EnemyController enemyPrefab;
        [SerializeField] private float positionRange;
        [SerializeField] private float yOffset;

        private Settings settings;
        private ViewSettings viewSettings;
        private GameObjectsPoolController gameObjectsPoolController;
        private Vector3[] positionsForSpawn;
        private List<PositionTime> timers = new List<PositionTime>();

        private void Start()
        {
            settings = SimpleInjector.Get<Settings>();
            viewSettings = SimpleInjector.Get<ViewSettings>();
            gameObjectsPoolController = SimpleInjector.Get<GameObjectsPoolController>();

            positionsForSpawn = new Vector3[settings.NumberOfEnemies];

            for (int i = 0; i < settings.NumberOfEnemies; i++)
            {
                positionsForSpawn[i] = new Vector3(Random.Range(-positionRange, positionRange),
                    yOffset, Random.Range(-positionRange, positionRange));

                CreateEnemy(i);
            }
        }

        private void Update()
        {
            for (int i = timers.Count - 1; i >= 0; i--)
            {
                timers[i].time -= Time.deltaTime;

                if (timers[i].time <= 0)
                {
                    CreateEnemy(timers[i].index);
                    timers.RemoveAt(i);
                }
            }
        }

        private void CreateEnemy(int indexOfPosition)
        {
            var enemyController = gameObjectsPoolController.CreateGameObject(enemyPrefab);
            enemyController.transform.SetParent(transform);
            enemyController.transform.position = positionsForSpawn[indexOfPosition];
            enemyController.SetData(settings.EnemyHp, indexOfPosition);
            enemyController.OnRemove += OnRemoveEnemy;
            enemyController.OnDamage += OnDamageEnemy;
        }

        private void OnRemoveEnemy(EnemyController enemyController)
        {
            enemyController.OnRemove -= OnRemoveEnemy;
            enemyController.OnDamage -= OnDamageEnemy;
            timers.Add(new PositionTime(enemyController.IndexPosition, settings.EnemyTimeRespawne));
            gameObjectsPoolController.RemoveGameObject(enemyController);
        }

        private void OnDamageEnemy(EnemyController enemyController, int damage)
        {
            var message = new CreateDamageTextMessage(damage, 
                enemyController.transform.position + viewSettings.DamageTextOffset);
            MessageContainer.SendMessage(message);
        }

        private class PositionTime
        {
            public int index;
            public float time;

            public PositionTime(int index, float time)
            {
                this.index = index;
                this.time = time;
            }
        }
    }
}