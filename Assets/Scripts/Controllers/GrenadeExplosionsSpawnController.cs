using PGSTestProject.Messages;
using PGSTestProject.Utils;
using UnityEngine;
using PGSTestProject.Model;

namespace PGSTestProject.Controllers
{
    public class GrenadeExplosionsSpawnController : MonoBehaviour
    {
        [SerializeField] private ExplosionController explosionPrefab;

        private Settings settings;
        private ViewSettings viewSettings;
        private GameObjectsPoolController gameObjectsPoolController;

        private void Start()
        {
            settings = SimpleInjector.Get<Settings>();
            viewSettings = SimpleInjector.Get<ViewSettings>();
            gameObjectsPoolController = SimpleInjector.Get<GameObjectsPoolController>();

            MessageContainer.AddSubscriber<CreateExplosionMessage>(SpawnExplosion);
        }

        public void SpawnExplosion(object obj)
        {
            var message = obj as CreateExplosionMessage;

            var grenadeData = settings.GetGrenadeDataByType(message.grenadeType);
            var controller = gameObjectsPoolController.CreateGameObject(explosionPrefab);
            controller.transform.position = message.position;
            controller.Explode(grenadeData.Radius, grenadeData.Damage, settings.ExplosionsSpeed);
            controller.OnRemove += RemoveExplosion;
        }

        private void RemoveExplosion(ExplosionController controller)
        {
            controller.OnRemove -= RemoveExplosion;
            gameObjectsPoolController.RemoveGameObject(controller);
        }
    }
}