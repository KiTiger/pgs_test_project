using System;
using UnityEngine;
using PGSTestProject.Model;

namespace PGSTestProject.Controllers
{
    [RequireComponent(typeof(Renderer))]
    public class IdleGrenadeController : MonoBehaviour
    {
        public event Action<IdleGrenadeController> OnRemove;

        private Material myMaterial;
        private GrenadeType currentGrenadeType;

        public GrenadeType CurrentGrenadeType => currentGrenadeType;

        private void Awake()
        {
            var renderer = GetComponent<Renderer>();
            var material = renderer.material;
            myMaterial = new Material(material);
            renderer.material = myMaterial;
        }

        public void SetData(Color color, GrenadeType grenadeType)
        {
            currentGrenadeType = grenadeType;
            myMaterial.SetColor("_Color", color);
        }

        public void Remove()
        {
            OnRemove?.Invoke(this);
        }
    }
}