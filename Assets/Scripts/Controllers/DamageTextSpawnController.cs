
using PGSTestProject.Messages;
using PGSTestProject.Utils;
using UnityEngine;

namespace PGSTestProject.Controllers
{
    public class DamageTextSpawnController : MonoBehaviour
    {
        [SerializeField] private DamageTextController textPrefab;

        private GameObjectsPoolController gameObjectsPoolController;

        private void Start()
        {
            gameObjectsPoolController = SimpleInjector.Get<GameObjectsPoolController>();

            MessageContainer.AddSubscriber<CreateDamageTextMessage>(Show);
        }

        public void Show(object obj)
        {
            var message = obj as CreateDamageTextMessage;

            var textController = gameObjectsPoolController.CreateGameObject(textPrefab);

            textController.transform.SetParent(transform);
            textController.transform.position = message.position;
            textController.Show(message.damage);
            textController.OnRemove +=  RemoveText;
        }

        public void RemoveText(DamageTextController textController)
        {
            textController.OnRemove -=  RemoveText;
            gameObjectsPoolController.RemoveGameObject(textController);
        }
    }
}