﻿using System;
using UnityEngine;
using PGSTestProject.Model;

namespace PGSTestProject.Controllers
{
    public class PlayerController : MonoBehaviour
    {
        public event Action<GrenadeType> OnGrenadeTake;
        public event Action OnNextGrenade;
        public event Action OnPrevGrenade;

        [SerializeField] private float speed;
        [SerializeField] private float turnSpeed;

        private Rigidbody _rigidbody;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            Cursor.lockState = CursorLockMode.Locked;
        }

        void Update()
        {
            Vector3 currentRotation = transform.rotation.eulerAngles;
            currentRotation.y += Input.GetAxis("Mouse X") * turnSpeed;
            _rigidbody.MoveRotation(Quaternion.Euler(currentRotation));

            if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.LeftArrow)) OnPrevGrenade?.Invoke();
            if (Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.RightArrow)) OnNextGrenade?.Invoke();
        }

        private void FixedUpdate()
        {
            int x = 0;
            int y = 0;

            if (Input.GetKey(KeyCode.D)) x += 1;
            if (Input.GetKey(KeyCode.A)) x -= 1;
            if (Input.GetKey(KeyCode.W)) y += 1;
            if (Input.GetKey(KeyCode.S)) y -= 1;

            _rigidbody.velocity = transform.TransformDirection(new Vector3(x * speed, 0, y *speed));
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "IdleGrenade")
            {
                GrenadeTake(other.gameObject.GetComponent<IdleGrenadeController>());
            }
        }

        private void GrenadeTake(IdleGrenadeController controller)
        {
            OnGrenadeTake?.Invoke(controller.CurrentGrenadeType);
            controller.Remove();
        }
    }

}