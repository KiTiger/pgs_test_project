using System;
using System.Collections.Generic;
using PGSTestProject.Utils;
using UnityEngine;

namespace PGSTestProject.Controllers
{
    public class GameObjectsPoolController : MonoBehaviour
    {
        private Dictionary<Type, Stack<MonoBehaviour>> listCreateObjectByType
            = new Dictionary<Type, Stack<MonoBehaviour>>();

        private void Awake()
        {
            SimpleInjector.Add(this);
        }

        public T CreateGameObject<T>(T component) where T : MonoBehaviour
        {
            Stack<MonoBehaviour> listComponents;
            if (listCreateObjectByType.TryGetValue(typeof(T), out listComponents))
            {
                if (listComponents.Count > 0)
                {
                    var item = (T)listComponents.Pop();
                    item.gameObject.SetActive(true);
                    return item;
                }
            }
            return Instantiate(component);
        }

        public void RemoveGameObject<T>(T component) where T : MonoBehaviour
        {
            if (component == null) return;

            Stack<MonoBehaviour> listComponents;
            if (!listCreateObjectByType.TryGetValue(typeof(T), out listComponents))
            {
                listComponents = new Stack<MonoBehaviour>();
                listCreateObjectByType.Add(typeof(T), listComponents);
            }

            listComponents.Push(component);
            component.transform.parent = transform;
            component.gameObject.SetActive(false);
        }
    }
}