﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PGSTestProject.Controllers
{
    [RequireComponent(typeof(Camera))]
    public class CameraController : MonoBehaviour
    {
        [SerializeField] private float turnSpeed;
        [SerializeField] private float maxAngel;
        [SerializeField] private float minAngel;

        private Camera currentCamera;
        private Transform parentTransform;

        private void Awake()
        {
            currentCamera = GetComponent<Camera>();
            parentTransform = transform.parent;
        }

        private void Update()
        {
            var angel = -Input.GetAxis("Mouse Y") * turnSpeed;

            parentTransform.eulerAngles += new Vector3(angel, 0, 0);

            Vector3 currentRotation = parentTransform.rotation.eulerAngles;
            if (currentRotation.x > 180) currentRotation.x = currentRotation.x - 360;
            currentRotation.x = Mathf.Clamp(currentRotation.x, minAngel, maxAngel);
            parentTransform.eulerAngles = currentRotation;
        }
    }
}