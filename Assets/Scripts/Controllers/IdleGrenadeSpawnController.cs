using System.Collections.Generic;
using PGSTestProject.Utils;
using UnityEngine;
using PGSTestProject.Model;

namespace PGSTestProject.Controllers
{
    public class IdleGrenadeSpawnController : MonoBehaviour
    {
        [SerializeField] private Transform[] spawnTransforms;
        [SerializeField] private IdleGrenadeController idleGrenadePrefab;

        private Settings settings;
        private ViewSettings viewSettings;
        private GameObjectsPoolController gameObjectsPoolController;
        private List<TransformTime> timers = new List<TransformTime>();

        private void Start()
        {
            settings = SimpleInjector.Get<Settings>();
            viewSettings = SimpleInjector.Get<ViewSettings>();
            gameObjectsPoolController = SimpleInjector.Get<GameObjectsPoolController>();

            foreach (var spawnTransform in spawnTransforms)
            {
                CreateGrenade(spawnTransform);
            }
        }

        private void Update()
        {
            for (int i = timers.Count - 1; i >= 0 ; i--)
            {
                timers[i].time -= Time.deltaTime;

                if(timers[i].time <= 0)
                {
                    CreateGrenade(timers[i].transform);
                    timers.RemoveAt(i);
                }
            }
        }

        private void CreateGrenade(Transform spawnTransform)
        {
            float[] probabilities = new float[settings.Grenades.Count];
            for (int i = 0; i < settings.Grenades.Count; i++)
            {
                probabilities[i] = settings.Grenades[i].Probability;
            }

            var currentGreandeData = settings.Grenades[ProbabilityUtils.GetIndexByProbability(probabilities)];
            var currentGreandeViewData = viewSettings.GetGrenadeViewDataByType(currentGreandeData.Type);

            var grenadeIdleController = gameObjectsPoolController.CreateGameObject(idleGrenadePrefab);

            grenadeIdleController.transform.SetParent(spawnTransform);
            grenadeIdleController.transform.localPosition = Vector3.zero;
            grenadeIdleController.SetData(currentGreandeViewData.Color, currentGreandeData.Type);
            grenadeIdleController.OnRemove += OnRemoveIdleGrenade;
        }

        private void OnRemoveIdleGrenade(IdleGrenadeController controller)
        {
            controller.OnRemove -= OnRemoveIdleGrenade;

            timers.Add(new TransformTime(controller.transform.parent, settings.GrenadeTimeRespawne));

            gameObjectsPoolController.RemoveGameObject(controller);

        }

        private class TransformTime
        {
            public Transform transform;
            public float time;
            
            public TransformTime(Transform transform, float time)
            {
                this.transform = transform;
                this.time = time;
            }
        }
    }
}