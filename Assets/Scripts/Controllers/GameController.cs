﻿using System.Collections;
using System.Collections.Generic;
using PGSTestProject.Utils;
using UnityEngine;
using PGSTestProject.Model;

namespace PGSTestProject.Controllers
{
    public class GameController : MonoBehaviour
    {
        [SerializeField] private PlayerController playerController;
        [SerializeField] private GrenadeThrowController grenadeThrowController;

        private GameState gameState;

        private void Awake() 
        {
            gameState = new GameState();

            SimpleInjector.Add(typeof(IGameState), gameState);

            playerController.OnGrenadeTake += gameState.AddGrenade;
            playerController.OnNextGrenade += gameState.NextGrenade;
            playerController.OnPrevGrenade += gameState.PrevGrenade;
            grenadeThrowController.OnTrowGrenade += gameState.RemoveGrenade;
        }
    }
}