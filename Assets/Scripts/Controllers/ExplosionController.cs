using System;
using UnityEngine;

namespace PGSTestProject.Controllers
{
    public class ExplosionController : MonoBehaviour
    {
        public event Action<ExplosionController> OnRemove;

        [SerializeField] private float startRadius = 0.1f;
        
        private float maxRadius;
        private float currntRadius;
        private int damage;
        private float speed;
        
        public int Damage => damage;

        public void Explode(float maxRadius, int damage, float speed)
        {
            transform.localScale = Vector3.one * startRadius;
            this.maxRadius = maxRadius;
            this.damage = damage;
            this.speed = speed;
            currntRadius = startRadius;
        }

        private void Update()
        {
            currntRadius += Time.deltaTime * speed;
            transform.localScale = Vector3.one * currntRadius;

            if(currntRadius >= maxRadius) OnRemove?.Invoke(this);
        }
    }
}