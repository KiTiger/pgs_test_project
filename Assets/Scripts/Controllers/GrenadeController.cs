using System;
using PGSTestProject.Messages;
using UnityEngine;
using PGSTestProject.Model;

namespace PGSTestProject.Controllers
{
    [RequireComponent(typeof(Renderer), typeof(Rigidbody))]
    public class GrenadeController : MonoBehaviour
    {
        public event Action<GrenadeController> OnBumped;

        private Rigidbody _rigidbody;
        private Material myMaterial;
        private GrenadeType currentGrenadeType;

        public GrenadeType CurrentGrenadeType => currentGrenadeType;
        public Rigidbody Rigidbody => _rigidbody;

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            var renderer = GetComponent<Renderer>();
            var material = renderer.material;
            myMaterial = new Material(material);
            renderer.material = myMaterial;
        }

        public void SetData(Color color, GrenadeType grenadeType)
        {
            currentGrenadeType = grenadeType;
            myMaterial.SetColor("_Color", color);
        }

        private void OnTriggerEnter(Collider other)
        {
            if(other.gameObject.tag == "Enemy" || other.gameObject.tag == "Plane")
            {
                var message = new CreateExplosionMessage(currentGrenadeType, transform.position);
                MessageContainer.SendMessage(message);
                OnBumped?.Invoke(this);
            }
        }
    }
}