using UnityEngine;
using PGSTestProject.Model;

namespace PGSTestProject.Messages
{
    public class CreateExplosionMessage
    {
        public GrenadeType grenadeType;
        public Vector3 position;

        public CreateExplosionMessage(GrenadeType grenadeType, Vector3 position)
        {
            this.grenadeType = grenadeType;
            this.position = position;
        }
    }
}