using System;
using System.Collections.Generic;

namespace PGSTestProject.Messages
{
    public static class MessageContainer
    {
        public delegate void CalledMethod(object obj);
        private static Dictionary<Type, List<CalledMethod>> subscribersByType 
            = new Dictionary<Type, List<CalledMethod>>();

        public static void AddSubscriber<T>(CalledMethod action)
        {
            var type = typeof(T);
            if (subscribersByType.ContainsKey(type))
            {
                subscribersByType[type].Add(action);
            }
            else
            {
                var list = new List<CalledMethod>();
                list.Add(action);
                subscribersByType.Add(type, list);
            }
        }

        public static void SendMessage<T>(T obj) where T: class
        {
            var type = typeof(T);

            if(!subscribersByType.ContainsKey(type)) return;

            foreach (var item in subscribersByType[type])
            {
                item?.Invoke(obj);
            }
        }
    }
}