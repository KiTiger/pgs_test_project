using UnityEngine;

namespace PGSTestProject.Messages
{
    public class CreateDamageTextMessage
    {
        public int damage;
        public Vector3 position;

        public CreateDamageTextMessage(int damage, Vector3 position)
        {
            this.damage = damage;
            this.position = position;
        }
    }
}