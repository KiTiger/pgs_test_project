using UnityEngine;

namespace PGSTestProject.Utils
{
    public static class ProbabilityUtils
    {
        public static int GetIndexByProbability(float[] probabilities)
        {
            float sum = 0;
            foreach (var prob in probabilities) sum += prob;
            float currentValue = Random.value * sum;
            float currentSum = 0;
            for (int i = 0; i < probabilities.Length; i++)
            {
                currentSum += probabilities[i];
                if(currentValue < currentSum) return i;
            }
            return probabilities.Length - 1;
        }
    }
}