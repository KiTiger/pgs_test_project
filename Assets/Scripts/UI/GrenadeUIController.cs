﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PGSTestProject.UI
{
    public class GrenadeUIController : MonoBehaviour
    {
        [SerializeField] private Image mainImage;
        [SerializeField] private Image selectedImage;
        [SerializeField] private Text numberText;

        public void SetData(Color color, bool isSelected, int number)
        {
            mainImage.color = color;
            selectedImage.enabled = isSelected;
            numberText.text = number.ToString();
        }

        public void ChangeNumber(int number)
        {
            numberText.text = number.ToString();
            gameObject.SetActive(number > 0);
        }

        public void Select()
        {
            selectedImage.enabled = true;
        }

        public void Unselect()
        {
            selectedImage.enabled = false;
        }
    }
}
