using System.Collections;
using System.Collections.Generic;
using PGSTestProject.Model;
using PGSTestProject.Utils;
using UnityEngine;

namespace PGSTestProject.UI
{
    public class GrenadeListUIController : MonoBehaviour
    {
        [SerializeField] private GrenadeUIController grenadeUIPrefab;

        private IGameState gameState;
        private ViewSettings viewSettings;

        private Dictionary<GrenadeType, GrenadeUIController> createdGreandeUICOntrollers 
            = new Dictionary<GrenadeType, GrenadeUIController>();

        private void Start()
        {
            gameState = SimpleInjector.Get<IGameState>();
            viewSettings = SimpleInjector.Get<ViewSettings>();

            gameState.OnChangeNumberOfGrenade += OnChangeNumberOfGrenade;
            gameState.OnChangeSelectedGrenade += OnChangeSelectedGrenade;
        }

        private void OnChangeSelectedGrenade(GrenadeType type)
        {
            foreach (var controller in createdGreandeUICOntrollers)
            {
                if(controller.Key == type) controller.Value.Select();
                else controller.Value.Unselect();
            }
        }

        private void OnChangeNumberOfGrenade(KeyValuePair<GrenadeType, int> keyValue)
        {
            if(createdGreandeUICOntrollers.ContainsKey(keyValue.Key))
            {
                createdGreandeUICOntrollers[keyValue.Key].ChangeNumber(keyValue.Value);
            }
            else
            {
                var controller = Instantiate(grenadeUIPrefab, Vector3.zero, Quaternion.identity, transform);
                controller.transform.localPosition = Vector3.zero;
                controller.transform.localRotation = Quaternion.identity;
                controller.transform.SetSiblingIndex((int)keyValue.Key);
                controller.SetData(viewSettings.GetGrenadeViewDataByType(keyValue.Key).Color, keyValue.Key == gameState.SelectedGrenade, keyValue.Value);
                createdGreandeUICOntrollers.Add(keyValue.Key, controller);
            }
        }

    }
}
