using System;
using System.Collections.Generic;

namespace PGSTestProject.Model
{
    public class GameState : IGameState
    {
        private SortedDictionary<GrenadeType, int> numberOfGrenades = new SortedDictionary<GrenadeType, int>();
        private GrenadeType selectedGrenade = GrenadeType.None;
        private event Action<KeyValuePair<GrenadeType, int>> onChangeNumberOfGrenade;
        private event Action<GrenadeType> onChangeSelectedGrenade;

        IDictionary<GrenadeType, int> IGameState.NumberOfGrenades => numberOfGrenades;
        GrenadeType IGameState.SelectedGrenade => selectedGrenade;
        event Action<KeyValuePair<GrenadeType, int>> IGameState.OnChangeNumberOfGrenade
        {
            add { onChangeNumberOfGrenade += value; }
            remove { onChangeNumberOfGrenade -= value; }
        }

        event Action<GrenadeType> IGameState.OnChangeSelectedGrenade
        {
            add { onChangeSelectedGrenade += value; }
            remove { onChangeSelectedGrenade -= value; }
        }

        public void AddGrenade(GrenadeType grenadeType)
        {
            if (numberOfGrenades.ContainsKey(grenadeType)) numberOfGrenades[grenadeType]++;
            else numberOfGrenades.Add(grenadeType, 1);

            if (selectedGrenade == GrenadeType.None) selectedGrenade = grenadeType;

            onChangeNumberOfGrenade?.Invoke(new KeyValuePair<GrenadeType, int>(grenadeType, numberOfGrenades[grenadeType]));
        }

        public void RemoveGrenade()
        {
            var grenadeType = selectedGrenade;

            numberOfGrenades[grenadeType]--;
            
            int amount = numberOfGrenades[grenadeType];

            if(numberOfGrenades[grenadeType] == 0)
            {
                numberOfGrenades.Remove(grenadeType);
                NextGrenade();
            }

            onChangeNumberOfGrenade?.Invoke(new KeyValuePair<GrenadeType, int>(grenadeType, amount));
        }

        public void NextGrenade()
        {
            if (selectedGrenade == GrenadeType.None) return;

            GrenadeType newselectedGrenade = selectedGrenade;
            do
            {
                newselectedGrenade = (GrenadeType)(((int)newselectedGrenade + 1) % ((int)GrenadeType.Amount));
                if (newselectedGrenade == selectedGrenade)
                {
                    if(!numberOfGrenades.ContainsKey(selectedGrenade)) selectedGrenade = GrenadeType.None;

                    return;
                }
            } while (!numberOfGrenades.ContainsKey(newselectedGrenade) || numberOfGrenades[newselectedGrenade] <= 0);

            selectedGrenade = newselectedGrenade;
            onChangeSelectedGrenade?.Invoke(selectedGrenade);
        }

        public void PrevGrenade()
        {
            if (selectedGrenade == GrenadeType.None) return;

            GrenadeType newselectedGrenade = selectedGrenade;
            do
            {
                newselectedGrenade = (GrenadeType)(((int)newselectedGrenade - 1) < 0
                    ? ((int)GrenadeType.Amount) - 1 : ((int)newselectedGrenade) - 1);
                if (newselectedGrenade == selectedGrenade) return;
            } while (!numberOfGrenades.ContainsKey(newselectedGrenade) || numberOfGrenades[newselectedGrenade] <= 0);

            selectedGrenade = newselectedGrenade;
            onChangeSelectedGrenade?.Invoke(selectedGrenade);
        }
    }
}