﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PGSTestProject.Model
{
    [CreateAssetMenu(fileName = "ViewSettings", menuName = "PGSTestProject/ViewSettings")]
    public class ViewSettings : ScriptableObject
    {
        [SerializeField] private List<GrenadeViewData> grenades;
        [SerializeField] private Vector3 damageTextOffset;
        [SerializeField] private int trajectoryNumberOfPoints = 100;
        [SerializeField] private int trajectoryRayDistance = 1000;

        public IList<GrenadeViewData> Grenades => grenades;
        public Vector3 DamageTextOffset => damageTextOffset;
        public int TrajectoryNumberOfPoints => trajectoryNumberOfPoints;
        public int TrajectoryRayDistance => trajectoryRayDistance;

        public GrenadeViewData GetGrenadeViewDataByType(GrenadeType type)
        {
            return grenades.Find(item => item.Type == type);
        }
    }

}