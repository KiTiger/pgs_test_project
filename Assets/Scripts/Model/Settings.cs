﻿using System.Collections.Generic;
using UnityEngine;

namespace PGSTestProject.Model
{
    [CreateAssetMenu(fileName = "Settings", menuName = "PGSTestProject/Settings")]
    public class Settings : ScriptableObject
    {
        [SerializeField] private List<GrenadeData> grenades;
        [SerializeField] private float grenadeTimeRespawne;
        [SerializeField] private float enemyTimeRespawne;
        [SerializeField] private int numberOfEnemies;
        [SerializeField] private int enemyHp;
        [SerializeField] private float explosionsSpeed;
        [SerializeField] private float grenadeThrowingForce = 20;

        public IList<GrenadeData> Grenades => grenades;
        public float GrenadeTimeRespawne => grenadeTimeRespawne;
        public float EnemyTimeRespawne => enemyTimeRespawne;
        public int NumberOfEnemies => numberOfEnemies;
        public int EnemyHp => enemyHp;
        public float ExplosionsSpeed => explosionsSpeed;
        public float GrenadeThrowingForce => grenadeThrowingForce;

        public GrenadeData GetGrenadeDataByType(GrenadeType neededType) => grenades.Find((item) => item.Type == neededType);
    }

}