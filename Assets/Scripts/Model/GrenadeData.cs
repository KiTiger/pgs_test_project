using System;
using UnityEngine;

namespace PGSTestProject.Model
{
    [Serializable]
    public class GrenadeData
    {
        [SerializeField] private GrenadeType type;
        [SerializeField] private float probability;
        [SerializeField] float radius;
        [SerializeField] int damage;

        public GrenadeType Type => type;
        public float Probability => probability;
        public float Radius => radius;
        public int Damage => damage;
        
    }
}