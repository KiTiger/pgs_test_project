using System;
using UnityEngine;

namespace PGSTestProject.Model
{
    [Serializable]
    public class GrenadeViewData
    {
        [SerializeField] private GrenadeType type;
        [SerializeField] private Color color;

        public GrenadeType Type => type;
        public Color Color => color;
        
    }
}