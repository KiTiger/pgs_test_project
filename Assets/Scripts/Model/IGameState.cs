using System;
using System.Collections.Generic;

namespace PGSTestProject.Model
{
    public interface IGameState
    {
        IDictionary<GrenadeType, int> NumberOfGrenades { get; }
        GrenadeType SelectedGrenade { get; }
        event Action<KeyValuePair<GrenadeType, int>> OnChangeNumberOfGrenade;
        event Action<GrenadeType> OnChangeSelectedGrenade;
    }
}